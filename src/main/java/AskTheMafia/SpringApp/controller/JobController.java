package AskTheMafia.SpringApp.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import AskTheMafia.AppWide.CentralSingleton;
import AskTheMafia.AppWide.Constants;
import AskTheMafia.AppWide.JobHolder;
import AskTheMafia.AppWide.LogObjectFactory;
import AskTheMafia.AppWide.MafiaMapper;
import AskTheMafia.Job.Job;
import AskTheMafia.Job.JobStatusRequest;
import AskTheMafia.Job.JobStatusResponse;
import AskTheMafia.Job.StopLossAndPriceChangeUpdateRequest;
import AskTheMafia.JobProcessor.IJobProcessor;
import AskTheMafia.Requests.BaseResponse;
import AskTheMafia.Requests.DeChunk;
import AskTheMafia.Requests.DeChunkResponse;
import AskTheMafia.Requests.Simple;

@RestController
public class JobController {

	@Autowired
	BaseResponse br;

	@Autowired
	DeChunk de;

	@Autowired
	Constants constants;

	@Autowired
	LogObjectFactory logfactory;

	@Autowired
	JobHolder jholder;

	@Autowired
	CentralSingleton cs;

	@Autowired
	@Qualifier("stop_loss_and_price_change_updater")
	private IJobProcessor slpcu;

	@Autowired
	MafiaMapper mapper_factory;

	private Boolean check_auth(Job req) {
		if (req.getAuth_t() == null || !req.getAuth_t().equals(constants.api_password)) {
			logfactory.get_lg(JobController.class).commit_debug_log("no auth:job controller");
		}
		return ((req.getAuth_t() != null && req.getAuth_t().equals(constants.api_password)));
	}

	@RequestMapping(value = "/get_job_status", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse get_job_status(@RequestBody JobStatusRequest req) {
		if (check_auth(req)) {
			return jholder.query_job_status(req.getJob_id());
		}
		return null;

	}

	@RequestMapping(value = "/cancel_job", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse cancel_job(@RequestBody JobStatusRequest req) {
		if (check_auth(req)) {
			return jholder.cancel_job(req.getJob_id());
		}
		return null;
	}

	@RequestMapping(value = "/cancel_all_jobs", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse cancel_all_jobs(@RequestBody JobStatusRequest req) {

		// cancel all futures from the job holder.
		if (check_auth(req)) {
			return jholder.cancel_all_jobs();
		}
		return null;
	}

	@RequestMapping(value = "/get_pending_jobs_count", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse get_pending_jobs_count(@RequestBody JobStatusRequest req) {
		if (check_auth(req)) {
			JobStatusResponse jsr = new JobStatusResponse();
			jsr.setJob_id(req.getJob_id());
			jsr.setPending_jobs(jholder.query_pending_jobs());
			return jsr;
		}
		return null;
	}

	@RequestMapping(value = "/get_semaphore_count", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse get_semaphore_count(@RequestBody JobStatusRequest req) {
		if (check_auth(req)) {
			JobStatusResponse jsr = new JobStatusResponse();
			jsr.setJob_id(req.getJob_id());
			jsr.setSemaphore_count(jholder.query_semaphore_count());
			return jsr;
		}
		return null;
	}

	@RequestMapping(value = "/pc_arrays", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JobStatusResponse update_arrs(@RequestBody StopLossAndPriceChangeUpdateRequest req)
			throws IOException {
		if (check_auth(req)) {
			logfactory.get_lg(JobController.class)
					.commit_info_log("received request as:" + mapper_factory.getMapper().writeValueAsString(req));
			JobStatusResponse jsr = jholder.add_job(req, slpcu);
			logfactory.get_lg(JobController.class).commit_debug_log(
					"returning jobstatus response:" + mapper_factory.getMapper().writeValueAsString(jsr));
			return jsr;
		}
		return null;
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object obj(@RequestBody Object obj){
		return new Object();
	}

	@RequestMapping(value = "/dechunk", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody DeChunkResponse dechunk(@RequestBody DeChunk dei) {
		System.out.println("array name---------------------");
		System.out.println(dei.getArray_name());
		
		de.setArray_name(dei.getArray_name());
		de.setExpected_digest(dei.getExpected_digest());
		de.setEarliest_newly_added_pair_index(dei.getEarliest_newly_added_pair_index());
		// so required array index is going to be passed in everytime or for
		// null ?
		// for the other arrays, its ok.
		
		if(dei.getRequired_large_array_index() != null){
			de.setRequired_large_array_index(dei.getRequired_large_array_index());
			de.process_for_open_open_or_close_close_array();
		}
		else{
			de.load_array_from_es_to_central_singleton();
		}
		
		DeChunkResponse dcr = new DeChunkResponse();
		dcr.setArray_name(de.getArray_name());
		dcr.setExpected_digest(de.getExpected_digest());
		dcr.compare_digest();
		System.out.println("expected digest: " + de.getExpected_digest());
		System.out.println("came to return comparison:" + dcr.getDigest_comparison());
		return dcr;
	}
	
	
	
	

	@RequestMapping(value = "/clear_singleton", method = RequestMethod.GET)
	public @ResponseBody BaseResponse respond() {
		try {
			System.out.println("------------ CLEARING SINGLETON -----------------");
			cs.query_time_pair_lookup_map.clear();
			br.setResult(true);
			System.out.println("cleare result is :" + br.getResult());
			//System.exit(1);
		} catch (Exception e) {
			br.setResult(false);
		}
		return br;
	}

}
