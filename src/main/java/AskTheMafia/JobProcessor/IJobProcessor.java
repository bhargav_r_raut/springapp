package AskTheMafia.JobProcessor;

import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;

import AskTheMafia.Job.Job;

public interface IJobProcessor {
	

	public abstract Future<String> process(Job j);
}
