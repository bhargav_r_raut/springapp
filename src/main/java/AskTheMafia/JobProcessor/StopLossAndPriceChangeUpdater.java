package AskTheMafia.JobProcessor;

import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import AskTheMafia.AppWide.Constants;
import AskTheMafia.AppWide.JobHolder;
import AskTheMafia.AppWide.StopLossAndPriceChangeArrays;
import AskTheMafia.Job.Job;
import AskTheMafia.Job.StopLossAndPriceChangeUpdateRequest;

@Component("stop_loss_and_price_change_updater")
public class StopLossAndPriceChangeUpdater implements IJobProcessor {

	@Autowired
	private StopLossAndPriceChangeArrays pcm;

	@Autowired
	private JobHolder jholder;

	@Autowired
	private Constants constants;

	public StopLossAndPriceChangeUpdater() {

	}

	@SuppressWarnings("static-access")
	@Async
	public Future<String> process(Job j) {
		// TODO Auto-generated method stub
		StopLossAndPriceChangeUpdateRequest su = (StopLossAndPriceChangeUpdateRequest) j;
		/***
		 * if the job id is long_running -> then it will sleep for 20 seconds.
		 * 
		 */
		if (su.getJob_id().equals("long_running")) {
			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/***
		 * if the job id is test, then it will do nothing.
		 */
		else if(su.getJob_id().equals("test")){
			
		}
		else{
			//now here we actually update something for a change.
			
		}
		
		try {
			return new AsyncResult<String>(constants.Job_success);
		} catch (Exception e) {
			return new AsyncResult<String>(constants.Job_failure);
		}

	}
}
