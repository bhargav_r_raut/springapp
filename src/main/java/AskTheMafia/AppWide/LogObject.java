package AskTheMafia.AppWide;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;



public class LogObject extends JSONObject {

	private static final String process_name = "process_name";
	private static final String entity_unique_name = "entity_unique_name";
	private static final String exception_object_key = "key";
	private static final String exception_object_value = "value";

	private Class<?> cls;

	public Class<?> getCls() {
		return cls;
	}

	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	private Logger logger;
	private JSONObject info_packet;
	private ErrorLog er;

	public ErrorLog getEr() {
		return er;
	}

	public void setEr(ErrorLog er) {
		this.er = er;
	}

	public LogObject(Class<?> cls) {
		super();
		set_logger_and_info_packet(cls);
	}

	public LogObject() {

	}

	public LogObject set_logger_and_info_packet(Class<?> cls) {
		this.logger = LoggerFactory.getLogger(cls);
		this.info_packet = new JSONObject();
		return this;
	}

	public void print_info_packet_and_logger() {
		System.out.println(this.info_packet);
		System.out.println(this.logger);
	}

	public void add_to_info_packet(String key, Object value) {
		try {
			this.info_packet.put(key, value);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LoggerFactory.getLogger(this.getClass()).error("", e);
			e.printStackTrace();
		}

	}

	public void commit_info_log(String status) {
		try {
			this.logger.info(status + this.toString(5) + "-- "
					+ this.info_packet.toString(5));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void commit_error_log(String status, Throwable e) {
		try {
			// assess the info packet for the entity unique name.
			// post that error to the
			this.logger.error(status + this.toString(5) + "-- "
					+ this.info_packet.toString(5), e);

			ErrorLog er = new ErrorLog(
					new GregorianCalendar().getTimeInMillis(), info_packet,
					status, e);
			this.er = er;

		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(1);
		}

	}

	public void commit_debug_log(String status) {

		this.logger.debug(status + this.toString(5) + "-- "
				+ this.info_packet.toString(5));

	}

	class ErrorLog {
		private String entity_unique_name;

		public String getEntity_unique_name() {
			return entity_unique_name;
		}

		public void setEntity_unique_name(String entity_unique_name) {
			this.entity_unique_name = entity_unique_name;
		}

		private Long session_id;

		public Long getSession_id() {
			return session_id;
		}

		public void setSession_id(Long session_id) {
			this.session_id = session_id;
		}

		private LinkedList<Map<String, Object>> info_packet;

		private String exception;

		public String getException() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}

		public LinkedList<Map<String, Object>> getInfo_packet() {
			return info_packet;
		}

		public void setInfo_packet(final JSONObject info_packet_j)
				throws JSONException {
			this.info_packet = new LinkedList<Map<String, Object>>();
			final Iterator<String> info_packet_iterator = info_packet_j.keys();
			while (info_packet_iterator.hasNext()) {
				final String key = info_packet_iterator.next();
				if (key.equals(entity_unique_name)) {
					setEntity_unique_name(info_packet_j.getString(key));
				}
				Map<String, Object> entry = new LinkedHashMap<String, Object>() {
					{
						put(exception_object_key, key);
						put(exception_object_value,
								String.valueOf(info_packet_j.get(key)));
					}
				};
				this.info_packet.add(entry);
			}
		}

		private String status;

		public ErrorLog(Long session_id, JSONObject info_packet_j,
				String status, Throwable e) throws Exception {

			this.session_id = session_id;
			setInfo_packet(info_packet_j);
			this.status = status;
			this.exception = e.getLocalizedMessage();

		}

		public void commit(Client client, Constants constants,ObjectMapper mapper) throws Exception {

			IndexRequest req = new IndexRequest();
			req.index("tradegenie_titan");
			req.type("_doc");
			req.source(mapper.writeValueAsBytes(this), XContentType.JSON);
			req.setRefreshPolicy(RefreshPolicy.IMMEDIATE);
			IndexResponse resp = client.index(req).actionGet();
			
			if(resp.getResult() == DocWriteResponse.Result.CREATED){
				System.out.println("ERROR LOG COMMITTED SUCCESSFULLY");
			}
			else{
				throw new Exception("failed to commit the error log.");
			}
		}

	}

}
