package AskTheMafia.AppWide;

import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;


@Component
public class StopLossAndPriceChangeArrays{

	private ConcurrentHashMap<String, int[]> parrs;

	public ConcurrentHashMap<String, int[]> getParrs() {
		return parrs;
	}

	public void setParrs(ConcurrentHashMap<String, int[]> parrs) {
		this.parrs = parrs;
	}

	public StopLossAndPriceChangeArrays() {
		this.parrs = new ConcurrentHashMap<String, int[]>();
	}

	/**
	 * after initialization, all arrays should be populated.
	 * 
	 */
	@PostConstruct
	public void populate_arrays() {
		System.out.println("populating the arrays post construct");
		getParrs().put("test", new int[10]);
	}
}
