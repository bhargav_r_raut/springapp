package AskTheMafia.AppWide;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.springframework.stereotype.Component;

@Component
public class Constants {
	
	public static final String elasticsearch_host_name = "localhost";
	public static final Integer elasticsearch_port_name = 9300;
	public static final String Job_success =  "success";
	public static final String Job_failure = "failure";
	public static final String Job_pending = "pending";
	public static final String Job_interrupted = "interrupted";
	public static final String Job_absent = "absent";
	public static final String ESTypes_error_log = "error_log";
	public static final String api_password = "aditya";
	/****
	 * 
	 * CONSTANTS SHARED FROM HOME APP.
	 * 
	 */
	public static final String Entity_arr = "arr";
	public static final String Entity_arr_name = "arr_name";

	public static final String Entity_chunk_number = "chunk_number";
	public static final String Entity_unique_name = "entity_unique_name";
	public static final String Entity_chunk_prefix = "C";
	public static final Integer Entity_chunk_size_for_close_close_and_open_open_price_change_arrays = 10000;

	public static final String Entity_first_newly_added_pair_index = "first";
	public static final String Entity_last_newly_added_pair_index = "last";
	public static final Integer CentralSingleton_total_datapoints = 3650;
	/****
	 * 
	 * SHARED CONSTANTS ENDS.
	 * 
	 */
	
	
	public Constants(){
		
	}
	
}	
