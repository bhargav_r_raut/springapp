package AskTheMafia.AppWide;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import AskTheMafia.Job.Job;
import AskTheMafia.Job.JobStatusResponse;
import AskTheMafia.JobProcessor.IJobProcessor;

@Component
public class JobHolder {

	@Autowired
	Constants constants;
	
	@Autowired LogObjectFactory log_factory;

	/***
	 * key consists of the session_start_time id from the local machine, plus
	 * the entity index value consists of false if pending, and true if
	 * completed.
	 * 
	 * 
	 */
	private ConcurrentHashMap<String, Future<String>> jobs;

	public ConcurrentHashMap<String, Future<String>> getJobs() {
		return jobs;
	}

	/***
	 * 
	 * semaphore the prevents more than one job being added at any given time.
	 * 
	 */
	private Semaphore sem;

	public Semaphore getSem() {
		return sem;
	}

	@Autowired
	private LogObjectFactory logfactory;

	public JobHolder() {
		this.jobs = new ConcurrentHashMap<String, Future<String>>();
		this.sem = new Semaphore(1);
	}

	/***
	 * @param j
	 * @param ijp
	 * @return : return the job id of the existing job in the jobs_map this
	 *         could be a older job, or it could be the current job if it is the
	 *         older job, then you know that the older job is still running. if
	 *         it is the current job then you know that the current job is being
	 *         processed.
	 */
	public JobStatusResponse add_job(Job j, IJobProcessor ijp) {
		JobStatusResponse jsr = new JobStatusResponse();
		LogObject lg = log_factory.get_lg(JobHolder.class);
		lg.add_to_info_packet("semaphore count is", getSem().availablePermits());
		lg.add_to_info_packet("pending jobs are :", getJobs().size());
		try {
			if (getSem().tryAcquire(1, TimeUnit.SECONDS)) {
				// if there are still jobs pending
				if (!getJobs().isEmpty()) {
					logfactory.get_lg(JobHolder.class).commit_info_log(
							"the jobs holder is not empty");
				} else {
					logfactory.get_lg(JobHolder.class).commit_debug_log(
							"added job in job holder");
					getJobs().put(j.getJob_id(), ijp.process(j));
				}
			} else {
				lg.add_to_info_packet("semaphore", "could not acquire");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		lg.commit_info_log("status");
		
		
		for (Map.Entry<String, Future<String>> entry : getJobs().entrySet()) {
			jsr.setJob_id(entry.getKey());
		}
		jsr.setPending_jobs(query_pending_jobs());
		jsr.setSemaphore_count(query_semaphore_count());		
		return jsr;
	}

	/**
	 * @iterate the jobs,
	 * @if a job is done -> mark it to be removed,
	 * @elseif its not done, cancel it. -@if : cancel is successfull mark the
	 *         job to be removed.
	 * 
	 * 
	 * 
	 * 
	 */
	public JobStatusResponse cancel_all_jobs() {
		ArrayList<String> jobs_to_remove = new ArrayList<String>();
		for (Map.Entry<String, Future<String>> entry : getJobs().entrySet()) {
			// cancel the task
			if (entry.getValue().isDone()) {
				jobs_to_remove.add(entry.getKey());
			}
			if (entry.getValue().cancel(true)) {
				jobs_to_remove.add(entry.getKey());
			}
		}

		for (String s : jobs_to_remove) {
			getJobs().remove(s);
		}

		JobStatusResponse jsr = new JobStatusResponse();
		jsr.setPending_jobs(query_pending_jobs());
		//release the semaphore.
		getSem().release();
		jsr.setSemaphore_count(query_semaphore_count());
		return jsr;

	}

	public int query_semaphore_count() {
		return getSem().availablePermits();
	}

	public int query_pending_jobs() {
		return getJobs().size();
	}

	/***
	 * @if the job does not exist
	 * 
	 *     return status of job as absent.
	 * 
	 * @elseif the job exists.
	 * 
	 * @if the future get succeeds without exception - return whatever status it
	 *     provides. release the semaphore remove the job from the job holder.
	 * 
	 * @elsif interrupted exception || execution exception return status
	 *        interrupted. release the semaphore remove the job from the job
	 *        holder
	 * 
	 * @elsif timeout exception return status as pending.
	 * 
	 * @finally assign the semaphore count and the pending job count.
	 */
	public JobStatusResponse query_job_status(String job_id) {
		JobStatusResponse response = new JobStatusResponse();
		response.setJob_id(job_id);
		if (getJobs().get(job_id) != null) {
			try {
				response.setStatus_response(getJobs().get(job_id).get(5,
						TimeUnit.SECONDS));
				getSem().release();
				getJobs().remove(job_id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setStatus_response(constants.Job_interrupted);
				getSem().release();
				getJobs().remove(job_id);
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setStatus_response(constants.Job_failure);
				getSem().release();
				getJobs().remove(job_id);
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				response.setStatus_response(constants.Job_pending);
				e.printStackTrace();
			}

		} else {
			response.setStatus_response(constants.Job_absent);

		}

		response.setPending_jobs(query_pending_jobs());
		response.setSemaphore_count(query_semaphore_count());

		return response;
	}

	/**
	 * first call query job status.
	 * 
	 * @if - the job exists -
	 * @if: the job is not done cancel the job(automatically sets the cancel
	 *      response) -@if: it was successfully cancelled - remove the job from
	 *      the jobholder -@else: dont do anything. - @else: the job is done
	 *      this means that it got done after the initial call to the query job
	 *      status, because otherwise that itself would have removed the job
	 *      from the job holder, and we would never even have entered this
	 *      condition. so call query job status again. then set cancel job
	 *      status to false - because job is done, so no need to cancel.
	 * 
	 * 
	 * @else - the job does not exist set the cancel response to false.
	 * 
	 * 
	 * @finally: call the semaphore count and pending jobs count respectively.
	 */
	public JobStatusResponse cancel_job(String job_id) {
		JobStatusResponse response = new JobStatusResponse();
		response.setJob_id(job_id);
		query_job_status(job_id);
		if (getJobs().get(job_id) != null) {
			if (!getJobs().get(job_id).isDone()) {
				response.setCancel_response(getJobs().get(job_id).cancel(true));
				if (response.getCancel_response()) {
					getJobs().remove(job_id);
					getSem().release();
				}
			} else {
				// the job is now done, this means that it got done after the
				// initial call to query job status
				// so call query job status again.
				query_job_status(job_id);
				response.setCancel_response(false);
			}
		} else {
			response.setCancel_response(false);
		}

		/**
		 * since query job status is called above it will initially set these
		 * two values, but they will be overwritten because we call these two
		 * methods again below.
		 * 
		 */
		response.setPending_jobs(query_pending_jobs());
		response.setSemaphore_count(query_semaphore_count());

		return response;
	}
	
	
	
}
