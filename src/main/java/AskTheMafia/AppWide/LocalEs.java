package AskTheMafia.AppWide;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;

import org.elasticsearch.common.settings.Settings;

import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocalEs {

	private Client client;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Autowired
	private Constants constants;

	@SuppressWarnings("static-access")
	public LocalEs() {
		// final ImmutableSettings.Builder settings = ImmutableSettings
		// .settingsBuilder();
		// settings.put("cluster.name","elasticsearch_xeon");

		// TransportClient transportClient = new TransportClient(settings);

		// transportClient = transportClient
		// .addTransportAddress(new InetSocketTransportAddress(
		// constants.elasticsearch_host_name,
		// constants.elasticsearch_port_name));

		// this.client = transportClient;

		///////////////////////////////////////////////////////////////////

		Settings settings = Settings.builder().put("client.transport.ignore_cluster_name", true)
				.put("transport.tcp.connect_timeout", "3s")
				// .put("es.http.timeout","5s")
				.build();
		TransportClient client;
		try {
			this.client = new PreBuiltTransportClient(settings).addTransportAddress(new TransportAddress(
					InetAddress.getByName(constants.elasticsearch_host_name), constants.elasticsearch_port_name));

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

}
