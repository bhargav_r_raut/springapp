package AskTheMafia.AppWide;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MafiaMapper {
	
	private ObjectMapper mapper;
	
	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public MafiaMapper(){
		this.mapper = new ObjectMapper();
	}
	
}
