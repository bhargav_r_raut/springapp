package AskTheMafia.AppWide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LogObjectFactory {
	
	@Autowired
	private LocalEs es;
	
	@Autowired
	private Constants constants;
	
	@Autowired
	private MafiaMapper mapper_factory;
	
	public LogObjectFactory(){
		
	}
	
	public LogObject get_lg(Class<?> cls){
		return new LogObject(cls);
	}
	
	public void commit_error_log(LogObject lg){
		try {
			lg.getEr().commit(es.getClient(),constants,mapper_factory.getMapper());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//get a logobject, and commit a log.
	//that can be done.
	//but how to wire in es.
	//first get a logobject, then add to it,
	//then call the method from the factory , passing in the logobject, and using the local es.
	

	//commit an error log.
	
	
	
}
