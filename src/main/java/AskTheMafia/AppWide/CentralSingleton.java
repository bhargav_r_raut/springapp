package AskTheMafia.AppWide;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import AskTheMafia.Requests.DeChunk;

@Component
public class CentralSingleton {

	@Autowired
	LocalEs localEs;

	public static ConcurrentHashMap<String, int[]> query_time_pair_lookup_map;

	public CentralSingleton() {
		this.query_time_pair_lookup_map = new ConcurrentHashMap<String, int[]>();
	}

	/*****
	 * 
	 * 
	 * 
	 * @return
	 */
	public synchronized int[] update_and_get_arr(String arr_name) {
		// is nt this decompressed already.
		int[] existing_arr = query_time_pair_lookup_map.getOrDefault(arr_name, new int[0]);
		/****
		 * Have no idea WTF I've done here, so I'm commenting it out.
		if (existing_arr.length > 0) {
			SearchResponse sr = localEs.getClient()
					.prepareSearch("tradegenie_titan")
					.setTypes("_doc")
					.setQuery(QueryBuilders.termQuery("arr_name", arr_name))
					.addSort("start_index", SortOrder.ASC)
					.setSize(100)
					.setFetchSource(true).execute().actionGet();
			for (SearchHit hit : sr.getHits().getHits()) {
				ArrayList<Integer> compressed_arrlist = (ArrayList<Integer>) hit.getSourceAsMap().get("arr");
				Integer[] wrapperArr = compressed_arrlist.toArray(new Integer[compressed_arrlist.size()]);
				int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
				int start_index = (Integer) hit.getSourceAsMap().get("start_index");
				int[] decomp_arr = DeChunk.get_decompressed_array_from_compressed_array(compressed_arr, 10000, 0);
				for(int i=0; i < decomp_arr.length; i++){
					Integer effective_index = start_index + i;
					if(effective_index >= existing_arr.length){
						
					}
					else{
						if(existing_arr[effective_index] == -99999 && decomp_arr[i]!= -99999){
							existing_arr[effective_index] = decomp_arr[i];
						}
					}
				}
			}
		}
		****/
		return existing_arr;
	}

}
