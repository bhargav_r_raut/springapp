package AskTheMafia.Requests;

import org.apache.commons.codec.digest.DigestUtils;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import AskTheMafia.AppWide.CentralSingleton;
import AskTheMafia.AppWide.Constants;

public class DeChunkResponse {
	@Autowired
	Client localEs;

	@Autowired
	Constants constants;
		
	@Autowired
	CentralSingleton cs;
	
	
	private String array_name;
	
	private String expected_digest;
	
	
	private Boolean digest_comparison;
	
	
	public Boolean getDigest_comparison() {
		return digest_comparison;
	}
	

	
	public void setDigest_comparison(Boolean digest_comparison) {
		this.digest_comparison = digest_comparison;
	}

	public String getArray_name() {
		return array_name;
	}
	

	
	public void setArray_name(String array_name) {
		this.array_name = array_name;
	}

	public String getExpected_digest() {
		return expected_digest;
	}
	
	
	public void setExpected_digest(String expected_digest) {
		this.expected_digest = expected_digest;
	}
	
	// for json.
	public DeChunkResponse(){
		
	}
	

	public DeChunkResponse(String array_name, String expected_digest){
		this.array_name = array_name;
		this.expected_digest = expected_digest;
	}
	
	public void compare_digest(){
		if(cs.query_time_pair_lookup_map.get(getArray_name()) == null){
			
			setDigest_comparison(false);
		}
		else{
			
			System.out.println("the array was there and it was:");
			System.out.println(cs.query_time_pair_lookup_map.get(getArray_name()));
			int[] arr = cs.query_time_pair_lookup_map.get(getArray_name());
			String s = convertToString(cs.query_time_pair_lookup_map.get(getArray_name()));
			System.out.println("came to convert to string");
			s = DigestUtils.sha256Hex(s);
			System.out.println("digest is: " + s);
			setDigest_comparison(s.equals(getExpected_digest()));
		}
	}
	
	final protected static char[] encoding = "0123456789ABCDEF".toCharArray();
    public static String convertToString(int[] arr) {
        char[] encodedChars = new char[arr.length * 4 * 2];
        for (int i = 0; i < arr.length; i++) {
            int v = arr[i];
            int idx = i * 4 * 2;
            for (int j = 0; j < 8; j++) {
                encodedChars[idx + j] = encoding[(v >>> ((7-j)*4)) & 0x0F];
            }
        }
        return new String(encodedChars);
    }
}
