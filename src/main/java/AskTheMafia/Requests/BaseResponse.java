package AskTheMafia.Requests;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class BaseResponse {
	
	private Boolean result;
	
	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public BaseResponse(){
		
	}
	
}
