package AskTheMafia.Requests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import AskTheMafia.AppWide.CentralSingleton;
import AskTheMafia.AppWide.Constants;
import AskTheMafia.AppWide.LocalEs;

import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.IntegerCODEC;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.differential.IntegratedBinaryPacking;
import me.lemire.integercompression.differential.IntegratedComposition;
import me.lemire.integercompression.differential.IntegratedIntegerCODEC;
import me.lemire.integercompression.differential.IntegratedVariableByte;

/****
 * this class should exist on the server remote. there we should call the es
 * client as RemoteEs.
 * 
 * @author bhargav
 *
 */
@Component
@Scope("prototype")
public class DeChunk {

	@Autowired
	LocalEs localEs;

	@Autowired
	Constants constants;

	@Autowired
	CentralSingleton cs;

	@Autowired
	RedisTemplate<String, Object> template;

	/****
	 * used in tests, the expected digest of the array, sent from the home
	 * computer. used in dechunkresponse, which checks if the array is same as
	 * what we expect it to be at home.
	 * 
	 */
	private String expected_digest;

	public String getExpected_digest() {
		return expected_digest;
	}

	public void setExpected_digest(String expected_digest) {
		this.expected_digest = expected_digest;
	}

	private int[] dechunked_large_array;

	public int[] getDechunked_large_array() {
		return dechunked_large_array;
	}

	public void setDechunked_large_array(int[] dechunked_large_array) {
		this.dechunked_large_array = dechunked_large_array;
	}

	/***
	 * the chunk id from which to dechunk
	 * 
	 */
	private Integer dechunk_from;

	public Integer getDechunk_from() {
		return dechunk_from;
	}

	public void setDechunk_from(Integer dechunk_from) {
		this.dechunk_from = dechunk_from;
	}

	/***
	 * the chunk id upto which and inclusive to dechunk.
	 */
	private Integer dechunk_to;

	public Integer getDechunk_to() {
		return dechunk_to;
	}

	public void setDechunk_to(Integer dechunk_to) {
		this.dechunk_to = dechunk_to;
	}

	/***
	 * the name of the array for which you need to dechunk.
	 * 
	 */
	private String array_name;

	public String getArray_name() {
		return array_name;
	}

	public void setArray_name(String array_name) {
		this.array_name = array_name;
	}

	/***
	 * index in the large open_open or close_close arrays which we need. this is
	 * optional parameter.
	 */
	private Integer required_large_array_index;

	public Integer getRequired_large_array_index() {
		return required_large_array_index;
	}

	public void setRequired_large_array_index(Integer required_large_array_index) {
		this.required_large_array_index = required_large_array_index;
	}

	private Integer current_large_array_size;

	public Integer getCurrent_large_array_size() {
		return current_large_array_size;
	}

	public void setCurrent_large_array_size(Integer current_large_array_size) {
		this.current_large_array_size = current_large_array_size;
	}

	private Integer latest_newly_added_pair_index;

	public Integer getLatest_newly_added_pair_index() {
		return latest_newly_added_pair_index;
	}

	public void setLatest_newly_added_pair_index(Integer latest_newly_added_pair_index) {
		this.latest_newly_added_pair_index = latest_newly_added_pair_index;
	}

	private Integer earliest_newly_added_pair_index;

	public Integer getEarliest_newly_added_pair_index() {
		return earliest_newly_added_pair_index;
	}

	public void setEarliest_newly_added_pair_index(Integer earliest_newly_added_pair_index) {
		this.earliest_newly_added_pair_index = earliest_newly_added_pair_index;
	}

	public DeChunk() {

	}

	/***
	 * used in tests and makes sense to use everywhere else as well.
	 * 
	 * @param array_name
	 * @param required_large_array_index
	 */
	public DeChunk(String array_name, Integer required_large_array_index) {
		this.array_name = array_name;
		this.required_large_array_index = required_large_array_index;
	}

	/***
	 * 
	 * USE THIS CONTRCUTOR FOR TESTS, SINCE IT PROVIDES THE EXPECTED_DIGEST
	 * STRING.
	 * 
	 */

	public DeChunk(String expected_digest, String array_name, Integer required_large_array_index) {
		this.expected_digest = expected_digest;
		this.array_name = array_name;
		this.required_large_array_index = required_large_array_index;
	}

	/****
	 * use this constructor for testing the sorted arrays 1) last_day_month_week
	 * array 2) bsl and tsl arrays.
	 * 
	 * @param expected_digest
	 * @param array_name
	 */
	public DeChunk(String expected_digest, String array_name) {
		this.expected_digest = expected_digest;
		this.array_name = array_name;
	}

	/***
	 * constructor used for loading the tsl and bsl arrays from the db into the
	 * centralsingleton.
	 * 
	 * @param array_name
	 */
	public DeChunk(String array_name) {
		this.array_name = array_name;
	}

	/***
	 * run the steps of dechunking.
	 */
	public void process_for_open_open_or_close_close_array() {
		// System.out.println("DOING INITIAL DEBUGGGIN---------------------");
		// debug_chunk_by_chunk(cs.update_and_get_arr(getArray_name()),
		// getArray_name());
		// System.out.println("INITIAL DEBUGGING COMPLETED
		// ---------------------");
		_size_of_array();
		_latest_existing_pair_id_for_large_array();
		calculate_chunk_from_and_to();
		build_dechunked_large_array();
		append_dechunked_array_to_existing_array();
	}

	// we just reload the stop loss arrays anyways.
	// now how does it work.
	// suppose we want an index, and the current length of hte array is less
	// than that index.
	// we have to initialize dechunk only after that.
	// then we have to check the latest pair id for that array
	// provided that it is more than or equal to whatever is being asked for.
	// so we have to calculate from which chunk to which chunk we need to
	// dechunk
	// then load those results from es and append to the current array.
	// for that a new array will have to
	// then reload all the stop loss arrays also for that entity.
	public void _latest_existing_pair_id_for_large_array() {
		SearchResponse sr = localEs.getClient().prepareSearch("tradegenie_titan").setTypes("_doc").setFetchSource(true)
				.setQuery(QueryBuilders.termQuery("arr_name", getArray_name()))
				.addSort("latest_newly_added_pair_index",
						SortOrder.DESC).setSize(1).execute().actionGet();

		for (SearchHit hit : sr.getHits().getHits()) {
			setLatest_newly_added_pair_index((Integer) hit.getSourceAsMap().get("latest_newly_added_pair_index"));
		}

		// System.out.println("latest newly added pair index");
		// System.out.println(getLatest_newly_added_pair_index());
	}

	public Boolean latest_existing_pair_id_greater_than_or_equal_to_required() {
		if (getLatest_newly_added_pair_index() != null) {
			return getLatest_newly_added_pair_index() >= getRequired_large_array_index();
		}
		return false;
	}

	public void _size_of_array() {
		setCurrent_large_array_size(cs.query_time_pair_lookup_map.getOrDefault(getArray_name(), new int[0]).length);
	}

	/****
	 * integer division gives whole number. multiplying by chunk size after that
	 * gives correct start index eg: 12000/10000 = 1 1*10000 = 10000 so start
	 * index correct is 10000.
	 */
	public void calculate_chunk_from_and_to() {

		// this depnds on the first newly downloaded pair id
		// that has to be added on the dechunk model
		// and sent in here.

		setDechunk_from((getEarliest_newly_added_pair_index()
				/ constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays)
				* constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays);

		setDechunk_to((getRequired_large_array_index()
				/ constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays)
				* constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays);

		System.out.println("dechunk from:" + getDechunk_from());
		System.out.println("dechunk to:" + getDechunk_to());

	}

	/****
	 * load the necessary chunks from es, and build multiple small uncompressed
	 * arrays out of them then use ArrayUtils.addAll to build one dechunked
	 * large uncompressed array.
	 */
	public void build_dechunked_large_array() {
		// get all arrays from es.
		// decompress them.
		// then
		// the size should be the maximum
		SearchResponse sr = localEs.getClient().prepareSearch("tradegenie_titan").setTypes("_doc").setFetchSource(true)
				.setQuery(QueryBuilders.boolQuery()
						.must(QueryBuilders.rangeQuery("start_index").from(getDechunk_from().intValue())
								.to(getDechunk_to().intValue()))
						.filter(QueryBuilders.termQuery("type", "arrays"))
						.filter(QueryBuilders.termQuery("arr_name", getArray_name())))
				.setSize((getDechunk_to()
						/ constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays)
						- (getDechunk_from()
								/ constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays)
						+ 1)
				.addSort("start_index", SortOrder.ASC).execute().actionGet();

		// System.out.println("array name is:" + getArray_name());
		// System.out.println("total chunks are:" +
		// sr.getHits().getHits().length);

		int[][] decompressed_arrs = new int[sr.getHits().getHits().length][];

		int hit_count = 0;
		int total_length = 0;
		for (SearchHit hit : sr.getHits().getHits()) {
			ArrayList<Integer> arr = (ArrayList<Integer>) hit.getSourceAsMap().get("arr");
			Integer[] wrapperArr = arr.toArray(new Integer[arr.size()]);
			Integer start_index = (Integer) hit.getSourceAsMap().get("start_index");
			// if (getArray_name().contains("close")) {
			// System.out.println("start index is:" + start_index);
			// }
			int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
			int[] decompressed_arr = get_decompressed_array_from_compressed_array(compressed_arr,
					constants.Entity_chunk_size_for_close_close_and_open_open_price_change_arrays, 0);
			decompressed_arrs[hit_count] = decompressed_arr;
			total_length += decompressed_arr.length;
			hit_count++;
		}

		// System.out.println("total length becomes: " + total_length);

		int start_index = 0;
		int[] summated_arr = new int[total_length];

		for (int[] k : decompressed_arrs) {
			// System.out.println("printing the first value of each chunk.");
			for (int i = 0; i < k.length; i++) {
				// if(i == 0){
				// System.out.println("indice : " + (i + start_index) + " :
				// value:" + k[i]);
				// }
				summated_arr[i + start_index] = k[i];
			}
			start_index += (k.length);
		}

		/***
		 * test code
		 * 
		 */
		/***	
		
		***/
		/***
		 * test code ends.
		 * 
		 */

		// take the array only so that we get only
		// upto the latest newly added pair index.
		// System.out.println("current large array size is:" +
		// getCurrent_large_array_size());
		// System.out.println("latest newly added pair index is:" +
		// (getLatest_newly_added_pair_index() + 1));
		// System.out.println("summated arr is null");
		// System.out.println(summated_arr == null);
		// System.out.println("summated arr length:" + summated_arr.length);
		// summated_arr = Arrays.copyOfRange(summated_arr,
		// getCurrent_large_array_size(),
		// getLatest_newly_added_pair_index() + 1);
		// the dechunking is done from the chunk start.
		// not necessarily from the
		// so it will be (get_large_array_size - get_dechunk_from)

		/*****
		 * logic: dechunk from 7000
		 * 
		 * but existing size is: 7003
		 * 
		 * so we should take the summated array from index 3(skip the first
		 * three) and upto
		 * 
		 * 
		 * latest_newly_added_index is: 8003
		 * 
		 * current_large_array_size is : 7003
		 * 
		 * so diff is 1000 but copyofrange to param is exclusive, so we add 1.
		 * 
		 * 
		 */

		// System.out.println("calculation is performed as follows:");
		// System.out.println("dechunk_from is:" + getDechunk_from());
		// System.out.println("current large array size is:" +
		// getCurrent_large_array_size());
		// System.out.println("latest newly added pair index is:" +
		// getLatest_newly_added_pair_index());

		// System.out.println("copyofrange from is: " +
		// (getCurrent_large_array_size() - getDechunk_from()));
		// System.out.println(
		// "copyofrange to is:" + (getLatest_newly_added_pair_index() + 1 -
		// getCurrent_large_array_size()));

		// this gives the summated arr till exactly the newly added index
		// latest one.
		//
		System.out.println("latest newly added pair index:" + getLatest_newly_added_pair_index());
		System.out.println("summated arr length before:" + summated_arr.length);
		summated_arr = Arrays.copyOfRange(summated_arr, 0,
				summated_arr.length + 1 - (getDechunk_to() + 10000 - 
						getLatest_newly_added_pair_index()));
		System.out.println("summated arr length after:" + summated_arr.length);

		setDechunked_large_array(summated_arr);
	}

	/***
	 * use arrayutils.addAll to append the dechunked large array to the existing
	 * array in centralsingleton.
	 */
	public void append_dechunked_array_to_existing_array() {
		// System.out.println("------------------CAME TO APPEND DECHUNKED
		// ARRAY--------");
		int[] existing_arr = cs.update_and_get_arr(getArray_name());

		// we had a start index from where we have reassembled the dechunked
		// array
		// that end index is the last index
		int[] final_decompressed_array = Arrays.copyOf(existing_arr,
				getLatest_newly_added_pair_index() + 1);
		
		System.out.println("final decompressed array length:" + final_decompressed_array.length);
		// summated array is starting with 0.
		System.out.println("earliest newly added:" + getEarliest_newly_added_pair_index());
		
			for (int i = 0; i < getDechunked_large_array().length; i++) {
				
				Integer index = i + getDechunk_from();
				final_decompressed_array[index] = getDechunked_large_array()[i];
			}
		

		// i think what is happening is that it is overwriting or appending some
		// how something wrong.
		// System.out.println("existing array length is:" +
		// existing_arr.length);
		// System.out.println("dechunked array length is:" +
		// getDechunked_large_array().length);
		// System.out.println("combined length:" + (existing_arr.length +
		// getDechunked_large_array().length));
		// int[] final_decompressed_array = Arrays.copyOf(existing_arr,
		// existing_arr.length + getDechunked_large_array().length);
		// for (int i = existing_arr.length; i <
		// final_decompressed_array.length; i++) {
		// final_decompressed_array[i] = getDechunked_large_array()[i -
		// existing_arr.length];
		// }
		cs.query_time_pair_lookup_map.put(getArray_name(), final_decompressed_array);
		debug_chunk_by_chunk(final_decompressed_array, getArray_name());
	}

	/****
	 * 
	 * use this function to load the tsl and bsl arrays from es into the
	 * centralsingleton.
	 * 
	 */
	public void load_array_from_es_to_central_singleton() {
		SearchResponse sr = localEs.getClient().prepareSearch("tradegenie_titan").setTypes("_doc").setFetchSource(true)
				.setSize(1).setQuery(QueryBuilders.termQuery("arr_name", getArray_name())).execute().actionGet();
		for (SearchHit hit : sr.getHits().getHits()) {
			System.out.println("the array name in question is:");
			System.out.println(getArray_name());

			ArrayList<Integer> arr = (ArrayList<Integer>) hit.getSourceAsMap().get("arr");
			Integer[] wrapperArr = arr.toArray(new Integer[arr.size()]);
			int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
			;
			// the size of this array is always the number of days in the
			// gmt_day_id_to_datestring_map.
			int[] decompressed_arr = get_decompressed_array_from_compressed_array(compressed_arr,
					constants.CentralSingleton_total_datapoints, 1);
			cs.query_time_pair_lookup_map.put(getArray_name(), decompressed_arr);
		}
	}

	/*****
	 * method used to compare cs large array ,with es, one chunk at a time.
	 * 
	 * 
	 */
	public void debug_chunk_by_chunk(int[] existing_array, String existing_array_name) {

		if (existing_array.length > 0) {
			System.out.println("existing array length is greater than zero,  name:" + existing_array_name);
			SearchResponse sr = localEs.getClient().prepareSearch("tradegenie_titan")
					.setQuery(QueryBuilders.termQuery("arr_name", existing_array_name))
					.addSort("start_index", SortOrder.ASC).setSize(100).setFetchSource(true).execute().actionGet();
			for (SearchHit hit : sr.getHits().getHits()) {
				ArrayList<Integer> compressed_arrlist = (ArrayList<Integer>) hit.getSourceAsMap().get("arr");
				Integer[] wrapperArr = compressed_arrlist.toArray(new Integer[compressed_arrlist.size()]);
				int[] compressed_arr = ArrayUtils.toPrimitive(wrapperArr);
				int start_index = (Integer) hit.getSourceAsMap().get("start_index");
				
				// basically the subsequent start index doesnt make sense.
				int[] decomp_arr = get_decompressed_array_from_compressed_array(compressed_arr, 10000, 0);
				if (start_index <= existing_array.length) {
					int[] existing_arr_slice = Arrays.copyOfRange(existing_array, start_index, start_index + 10000);
					if (!Arrays.equals(decomp_arr, existing_arr_slice)) {
						for (int i = 0; i < decomp_arr.length; i++) {
							if (start_index + i <= (existing_array.length - 1)) {
								if (existing_arr_slice[i] != decomp_arr[i]) {
									System.out.println("at start index: " + start_index
											+ " there is inequality :with array:" + existing_array_name);
									System.out.println("at index, there is inequality:" + i);
									System.out.println("es slice: " + decomp_arr[i]);
									System.out.println("singleton slice: " + existing_arr_slice[i]);
									break;
									// System.exit(1);
								}
							} else {
								 System.out.println("at index, length exceeds:" + i);
							}
						}
					} else {
						System.out.println("start index :" + start_index + " is ok(existing vs es)");
					}
				}
			}

		}
	}

	/****
	 * 
	 * METHOD COPIED FROM PAIR_ARRAY.
	 * 
	 */
	/****
	 * given a compressed array, and its type,(whether unsorted or sorted)
	 * returns a decompressed array.
	 * 
	 * 
	 * @param compressed_arr
	 * @param expected_length_of_decompressed_array
	 * @param type
	 *            : 0 -> unsorted, 1 -> sorted.
	 * @return
	 */
	public static int[] get_decompressed_array_from_compressed_array(int[] compressed_arr,
			int expected_length_of_decompressed_array, Integer type) {

		int[] recovered = new int[expected_length_of_decompressed_array];

		if (type.equals(0)) {
			// unsorted
			IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
			IntWrapper recoffset = new IntWrapper(0);
			codec.uncompress(compressed_arr, new IntWrapper(0), compressed_arr.length, recovered, recoffset);
			// System.out
			// .println("invoking decompression of unsorted array and returned
			// \n");

		} else {
			// sorted
			IntegratedIntegerCODEC codec = new IntegratedComposition(new IntegratedBinaryPacking(),
					new IntegratedVariableByte());
			recovered = new int[expected_length_of_decompressed_array];
			IntWrapper recoffset = new IntWrapper(0);
			codec.uncompress(compressed_arr, new IntWrapper(0), compressed_arr.length, recovered, recoffset);
			// System.out
			// .println("invoking decompression of sorted array and returned
			// \n");
		}

		return recovered;

	}

}
